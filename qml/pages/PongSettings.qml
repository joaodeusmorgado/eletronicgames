import QtQuick 2.0
import "components"

Rectangle {
    id: pongSettings

    color: "black"

    property alias intputBallVelocity: inputBallVelocity.textMG
    property alias intputPaddleSize: inputPaddleSize.textMG
    property alias inputMatchPlays: inputMatchPlays.textMG
    property alias inputPaddleAIVelocity: inputPaddleAIVelocity.textMG

    signal restoreDefautls()

    Flickable {
        width: parent.width; height: parent.height
        contentWidth: parent.width; contentHeight: (textInpuHeight+defaultMargins)*5

        //ball velocity------------------------------------------------------
        Text {
            id: textBallVelocity
            width: textMatchPlays.width
            height: textInpuHeight
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: defaultMargins
            text: qsTr("Ball velocity:")
            font.pixelSize: textSize
            color: "white"
            verticalAlignment: Text.AlignVCenter
        }

        TextInputMG {
            id: inputBallVelocity
            //onTextAccepted: console.log(textMG)
            anchors.verticalCenter: textBallVelocity.verticalCenter
            anchors.left: textBallVelocity.right
            anchors.margins: defaultMargins

        }
        //ball velocity------------------------------------------------------


        //Paddle PC velocity------------------------------------------------------
        Text {
            id: texPaddleAIVelocity
            width: textMatchPlays.width
            height: textInpuHeight
            anchors.left: parent.left
            anchors.top: inputBallVelocity.bottom
            anchors.margins: defaultMargins
            text: qsTr("Paddle PC velocity:")
            font.pixelSize: textSize
            color: "white"
            verticalAlignment: Text.AlignVCenter
        }

        TextInputMG {
            id: inputPaddleAIVelocity
            //onTextAccepted: console.log(textMG)
            anchors.verticalCenter: texPaddleAIVelocity.verticalCenter
            anchors.left: texPaddleAIVelocity.right
            anchors.margins: defaultMargins

        }
        //Paddle PC velocity------------------------------------------------------

        //paddle size------------------------------------------------------
        Text {
            id: textPaddleSize
            width: textMatchPlays.width
            height: textInpuHeight
            anchors.left: parent.left
            anchors.top: texPaddleAIVelocity.bottom
            anchors.margins: defaultMargins
            text: qsTr("Paddle size:")
            font.pixelSize: textSize
            color: "white"
            verticalAlignment: Text.AlignVCenter
        }

        TextInputMG {
            id: inputPaddleSize
            //onTextAccepted: console.log(textMG)
            anchors.verticalCenter: textPaddleSize.verticalCenter
            anchors.left: textPaddleSize.right
            anchors.margins: defaultMargins

        }
        //paddle size------------------------------------------------------


        //matchPlays------------------------------------------------------
        Text {
            id: textMatchPlays
            anchors.left: parent.left
            anchors.top: textPaddleSize.bottom
            anchors.margins: defaultMargins
            text: qsTr("Matches per game: ")
            height: textInpuHeight
            font.pixelSize: textSize
            color: "white"
            verticalAlignment: Text.AlignVCenter
        }

        TextInputMG {
            id: inputMatchPlays
            //onTextAccepted: console.log(textMG)
            anchors.verticalCenter: textMatchPlays.verticalCenter
            anchors.left: textMatchPlays.right
            anchors.margins: defaultMargins
        }
        //matchPlays------------------------------------------------------

    }//Flickable


    ButtonEG {
        id: restoreDefault
        width: 20 * mm
        height: 10 * mm
        anchors.bottom: parent.bottom
        anchors.right: btnSettings.left
        anchors.margins: defaultMargins
        text: qsTr("Restore\nDefault")
        textColor: "white"
        backgroundColor: "black"
        onButtonClick: restoreDefautls()
    }


    signal ok()
    ButtonEG {
        id: btnSettings
        width: 20 * mm
        height: 10 * mm
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: defaultMargins
        text: qsTr("Ok")
        textColor: "white"
        backgroundColor: "black"
        onButtonClick: ok()
    }

}
