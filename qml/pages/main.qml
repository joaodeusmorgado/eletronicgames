import QtQuick 2.2
import QtQuick.Window 2.2
import Qt.labs.settings 1.0 //for android OS
import "components"

Window {
    id: mainRoot
    visible: true
    //width: 640
    //height: 480
    width: Qt.platform.os === "android" ? Screen.width : 640
    height: Qt.platform.os === "android" ? Screen.height : 480

    title: qsTr("Electronic Games")
    color: "black"
    //visibility: Window.FullScreen
    visibility: Qt.platform.os === "android" ? Window.FullScreen : Window.AutomaticVisibility

    //Change program size between Desktop and Mobile.
    //property real calibrationFactor: Qt.platform.os === "android"  || Qt.platform.os === "ios" ?
      //                                   0.6 : 1.2
    //Define buttons and text size in milimeters.
    property real mm: Screen.pixelDensity//* calibrationFactor

    //Component.onCompleted: {
    //    console.log("calibrationFactor: "+calibrationFactor)
      //  console.log("mm: "+mm)
    //}


    //Define button size.
    /*property real buttonHeight: 16 * mm
    property real buttonWidth: 36 * mm

    //Define margins.
    property real defaultMargins: 2 * mm

    //Define text size.
    property int textSize: 8*mm
    property int textTitleSize: 15*mm*/

    //----------------
    //Define button size.
    property real buttonHeight: 10 * mm
    property real buttonWidth:  30 * mm
    property real textInpuHeight: 9 * mm

    //Define margins.
    property real defaultMargins: 2 * mm

    //Define text size.
    property int textSize: 4*mm
    property int textTitleSize: 10*mm

    property int fps: 60
    property int fps_ms: 1000/fps

    Settings { //for android and desktop
        id: settings
        //property alias calibrationFactor: mainRoot.calibrationFactor
        property alias velocity: statesManager.velocity
        property alias velocityInitial: statesManager.velocityInitial
        property alias velocityAI: statesManager.velocityAI
        property alias paddleSize: statesManager.paddleSize
        property alias scoreMax: statesManager.scoreMax
        property alias matchPlays: statesManager.matchPlays
    }

    /*statesManager.pong.velocity = settings.value("velocity", 1*mm.toFixed(2))
    statesManager.pong.velocityInitial = settings.value("velocityInitial", 1*mm.toFixed(2))
    statesManager.pong.velocityAI = settings.value("velocityAI", 1*mm.toFixed(2))
    statesManager.pong.paddleSize = settings.value("paddleSize", 12*mm.toFixed(2))
    statesManager.pong.scoreMax = settings.value("scoreMax", 0)
    statesManager.pong.matchPlays = settings.value("matchPlays", 10)
*/

    StatesManager{
        id: statesManager
        anchors.fill: parent
    }
}
