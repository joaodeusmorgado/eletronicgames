import QtQuick 2.0
import "components"

Rectangle {
    width: mainRoot.width
    height: mainRoot.height
    color: "black"

    signal back()

    Text {
        id: title
        text: qsTr("Electronic Games - Classic Games")
        color: "white"
        font.pixelSize: textSize
        anchors.left: parent.left
        anchors.leftMargin: defaultMargins * 5
    }

    Rectangle {
        id: settings
        color: "black"
        border.color: "white"
        border.width: defaultMargins
        radius: defaultMargins * 2

        //Define Rectangle Margins.
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: title.bottom
        anchors.left: parent.left
        anchors.topMargin: defaultMargins
        anchors.margins: defaultMargins * 5

        //Define text.
        Text {
            id: text
            text: qsTr("Credits:"
                       +"\nOriginal author - André Morgado"
                       +"\nProgrammer - André Morgado"
                       +"\nProgrammer - João Morgado"
                       +"\n\nVersion: 0.2."
                       +"\nThis program was developed with Qt5.")
            color: "white"
            font.pixelSize: textSize

            //Define Text Top Margins.
            anchors.top: parent.top
            anchors.topMargin: defaultMargins * 2

            //Define Text Left Margins.
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins * 4
        }

        /*ButtonEG {
            id: buttonBack
            onButtonClick: back()
            text: qsTr("1)Back")
            textColor: "black"

            //Define Button Back Margins.
            anchors.bottom: parent.bottom
            anchors.bottomMargin: defaultMargins * 3
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins * 3
        }*/
    }

    MouseArea{
        anchors.fill: parent
        onClicked: back()
    }
}
