import QtQuick 2.0
import "components"

Rectangle {
    width: mainRoot.width
    height: mainRoot.height
    color: "black"

    signal back()
    signal pong()

    Text {
        id: title
        text: qsTr("Electronic Games - Classic Games")
        color: "white"
        font.pixelSize: textSize
        anchors.left: parent.left
        anchors.leftMargin: defaultMargins * 5
    }

    //Size button variables.
    property real btnWidth: buttonWidth
    property real btnHeight: buttonHeight * 0.9
    //Size button variables end.

    Column {
        spacing: defaultMargins
        //Define button margins.
        anchors.top: title.bottom
        anchors.left: buttonBack.left
        anchors.topMargin: defaultMargins * 1.5
        //Define button margins end.

        ButtonEG {
            id: buttonPong
            text: qsTr("1)Pong")
            textColor: "black"
            width: btnWidth; height: btnHeight
            onButtonClick: pong()
        }

        Text {
            text: qsTr("More to come ... maybe 🙂")
            color: "white"
            font.pixelSize: textSize
        }

        /*ButtonEG {
            id: buttonBreakout
            text: qsTr("2)Breakout")
            textColor: "black"
            width: btnWidth; height: btnHeight
        }

        ButtonEG {
            id: buttonSnakegame
            text: qsTr("3)Snake")
            textColor: "black"
            width: btnWidth; height: btnHeight
        }

        ButtonEG {
            id: buttonBobinhos
            text: qsTr("4)Asteroids")
            textColor: "black"
            width: btnWidth; height: btnHeight
        }*/

    }

    ButtonEG {
        id: buttonBack
        onButtonClick: back()
        text: qsTr("Back")
        textColor: "black"

        //Define button margins.
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins * 3
        anchors.left: parent.left
        anchors.leftMargin: defaultMargins * 3
        //Define button margins end.
    }

}
