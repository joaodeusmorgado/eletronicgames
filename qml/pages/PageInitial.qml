import QtQuick 2.0
import "components"

Item {
    width: mainRoot.width
    height: mainRoot.height

    signal start()
    signal settings()

    Text {
        id: title
        text: qsTr("Electronic Games")
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: parent.height * 0.1
        font.pixelSize: textTitleSize
    }

    ButtonEG {
        id: buttonStart
        text: qsTr("Start")
        textColor: "black"
        //anchors.horizontalCenter: parent.horizontalCenter
        //anchors.top: title.bottom
        //anchors.topMargin: title.height * 0.5
        anchors.centerIn: parent
        onButtonClick: start()
    }

    ButtonEG {
        id: buttonSettings
        text: qsTr("About")
        textColor: "black"
        anchors.top: buttonStart.bottom
        anchors.left: buttonStart.left
        anchors.topMargin: defaultMargins * 1.5
        onButtonClick: settings()
    }

}
