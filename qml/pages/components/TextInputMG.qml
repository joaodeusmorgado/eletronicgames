//import QtQuick 2.0
import QtQuick 2.0

// Line edit
Rectangle {
    id: m_textEdit
    width: buttonWidth //100
    height: textInpuHeight//_DpiManager.getPixelsFromCentimeters(mainRoot.btnSize)
    radius: height * 0.4

    border.color: "lightgrey"
    color: readOnly ? "grey" : "white"

    property alias readOnly: m_text.readOnly
    property alias textMG: m_text.text
    property alias accepInput: m_text.acceptableInput
    property alias inputFocus: m_text.focus
    property alias hasFocus: m_text.cursorVisible
    property int _margins: 2+ radius

    signal textAccepted()
    //signal textEdited()
    signal focusActivated()
    signal focusDeactivated()
    signal keyUpPress()
    signal keyDownPress()

    TextInput {
        id: m_text
        height: buttonHeight
        font.pixelSize: textSize
        verticalAlignment: TextInput.AlignVCenter
        //color: "#151515"; selectionColor: "green"
        //font.pixelSize: 16; font.bold: true
        anchors.fill: parent
        anchors.leftMargin: _margins
        anchors.rightMargin: _margins
        //focus: false
        //property string m_solution
        //readOnly: true
        selectByMouse : true
        clip: true
        text: ""
        onAccepted: textAccepted()
        //onTextEdited: textEdited()
        onFocusChanged: {
            focus === true ? focusActivated() : focusDeactivated()
        }
        Keys.onPressed: event.key === Qt.Key_Up ? keyUpPress() : event.key === Qt.Key_Up ? keyDownPress() : ""
        onCursorPositionChanged: console.log("cursor position is: "+cursorPosition)
    }

    onFocusActivated: console.log("focus activated")
    onFocusDeactivated: console.log("focus lost")
    //onTextAccepted: console.log("textAccepted is: "+textMG)

    /*function clear(){
        m_text.remove(m_text.text.length-1, m_text.text.length)
    }*/

    function insert(str) {
        m_text.insert(m_text.cursorPosition, str)
    }

    function removeBackAtCursorPosition() {
        m_text.remove(m_text.cursorPosition-1, m_text.cursorPosition)
    }

    function cursorMoveLeft() {
        m_text.focus = true
        Qt.inputMethod.hide()
        m_text.cursorPosition -=1
    }

    function cursorMoveRight() {
        m_text.focus = true
        Qt.inputMethod.hide()
        m_text.cursorPosition +=1
    }

}
