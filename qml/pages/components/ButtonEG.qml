import QtQuick 2.0

Rectangle {
    id: buttonEG
    width: buttonWidth
    height: buttonHeight
    radius: buttonHeight * 0.25
    border.width: 1
    border.color: "white"
    color: _isBtnPressed ? backgroundColorPressed : backgroundColor

    //Button Properties.
    property int size: buttonWidth//_DpiManager.getPixelsFromCentimeters(mainRoot.buttonHeight)
    property alias text: buttonLabel.text
    property alias textFormat: buttonLabel.textFormat
    property alias bold: buttonLabel.font.bold
    property alias textColor: buttonLabel.color
    property color backgroundColor: "white"
    property color backgroundColorPressed: "grey"
    property bool _isBtnPressed: false
    //Button Properties end.

    signal buttonClick()

    Text {
        id: buttonLabel

        anchors.fill: parent
        anchors.margins: 0.4
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        fontSizeMode: Text.Fit
        minimumPointSize: 2
        font.family: "Arial"
        font.bold: false
        font.pixelSize: textSize
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: {
            buttonClick()
        }
        onPressed: {
            _isBtnPressed = true
        }
        onReleased: {
            _isBtnPressed = false
        }
        onCanceled: {
            _isBtnPressed = false
        }
    }
}
