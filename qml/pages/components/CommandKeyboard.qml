   import QtQuick 2.0

Item {
    id: root
    //width: 1
    //height: 1
    focus: true

    property real velocityOnKeyPress: 2*mm

    property bool canFire: false
    property bool spaceKey: false
    property bool leftKey: false
    property bool rightKey: false
    property bool upKey: false
    property bool downKey: false
    property bool isPressed: false
    property bool isFlying: false

    signal fly() //used for flying the hero
    signal walk() // used for walking the hero
    signal move(real cx, real cy) // used for moving the spaceship
    signal shotFired()
    signal startWalking()
    signal stopWalking()
    signal jump()
    //signal jumpLeft()
    //signal jumpRight()
    signal direction(int direct)// -1->left ; 1->right
    signal swordDefenseStart()
    signal swordDefenseStop()
    signal swordAttack()
    signal keyPressed_F()
    signal keyReleased_F()

    signal startFlying()
    signal stopFlying()

    onKeyPressed_F: startFlying()
    onKeyReleased_F: stopFlying()

    onStartFlying: isFlying = true
    onStopFlying: isFlying = false


    Keys.onPressed: {
        isPressed = true
        //console.log("key pressed")
        if (event.isAutoRepeat)
            return;

        //console.log("key pressed")

        //---shot fire-----------------------------
        if (event.key === Qt.Key_Space) {
            //shotFired()
            canFire = true;
            spaceKey = true;
            event.accepted = true;
            //return;
        }


        if (event.key === Qt.Key_Right) {
            rightKey = true;
            direction(1)
            startWalking()
            event.accepted = true;
            return;
        }

        if (event.key === Qt.Key_Left) {
            leftKey = true;
            direction(-1)
            startWalking()
            event.accepted = true;
            return;
        }

        if (event.key === Qt.Key_Space) {
            spaceKey = true
            event.accepted = true;
            return;
        }

        if (event.key === Qt.Key_Up) {
            upKey = true;
            event.accepted = true;
            return;
        }

        if (event.key === Qt.Key_Down) {
            downKey = true;
            event.accepted = true;
            return;
        }

        if (event.key === Qt.Key_Shift){
            swordDefenseStart()
            return;
        }

        if (event.key === Qt.Key_F){
            keyPressed_F()
            return;
        }

        if (event.key === Qt.Key_Control){
            swordAttack()
            return;
        }

    } //Keys.onPressed

    Keys.onReleased: {
        isPressed = false

        if (event.isAutoRepeat)
            return;

        if (event.key === Qt.Key_Space) {
            canFire = false;
            spaceKey = false;
            event.accepted = true;
        }


        if (event.key === Qt.Key_Right) {
            rightKey = false;
            stopWalking()
            event.accepted = true;
        }
        if (event.key === Qt.Key_Left) {
            leftKey = false;
            stopWalking()
            event.accepted = true;
        }
        if (event.key === Qt.Key_Up) {
            upKey = false;
            event.accepted = true;
        }
        if (event.key === Qt.Key_Down) {
            downKey = false;
            event.accepted = true;
        }

        if (event.key === Qt.Key_Shift){
            swordDefenseStop()
            return;
        }

        if (event.key === Qt.Key_F){
            keyReleased_F()
            return;
        }

        //console.log("release:"+event.key)
       // event.accepted = true;
    } //Keys.onReleased


    Timer {
        id: timer
        interval: fps_ms
        running: isPressed = true
        repeat: true
        onTriggered: {

            if (spaceKey) {
                if (canFire) {
                  shotFired()
                  canFire = false
                }
                jump()
            }

            if (leftKey) {
              walk()
              move(-velocityOnKeyPress,0)
              //return 1;
            }
            if (rightKey) {
                walk()
                move(velocityOnKeyPress,0)
                //return 1;
            }
            if (upKey) {
                //console.log("timmer up key")
                move(0,velocityOnKeyPress)
                jump()
                //return 1;
            }
            if (downKey) {
                //console.log("timmer down key")
                move(0, -velocityOnKeyPress)
                //return 1;
            }

            if (isFlying) {
                console.log("keyboard f fly key")
                fly()
            }
        }
    }//Timer
}
