import QtQuick 2.0
import QtMultimedia 5.6
import "components"

Rectangle {
    id: pong
    //width: mainRoot.width
    //height: mainRoot.height
    color: "black"
    property real yMain: height*0.5

    property int score: 0
    property int scoreMax: 0
    property int player1Victories: 0
    property int playerPCVictories: 0

    property real velocity: 1*mm.toFixed(2)         //ball velocity
    property real velocityInitial: 1*mm.toFixed(2)  //ball Initial velocity

    onVelocityChanged: console.log("velocity: "+velocity)

    property real velocityAI: 1.2*mm.toFixed(2)

    property real performanceX1: player1.x+player1.width*1.2
    property real performanceX2: playerPC.x-playerPC.width*0.2

    property alias player1: player1

    signal leftWallCollision()
    signal rightWallCollision()

    signal player1Win()
    signal playerPCWin()

    signal start()
    signal end()
    signal quit()

    onStart: {
        score = player1Victories = playerPCVictories = 0
        //canvas.requestPaint()
        //commandKeyb.move.connect(player1.move)
    }

    onEnd: {
        //commandKeyb.move.disconnect(player1.move)
    }


    onVisibleChanged: visible === true ? start() : end()


    /*Canvas {
        id: canvas
        width: parent.width
        height: parent.height

        property color color: "red"
        property var ctx
        onPaint: {
            //Dashed Line.
            ctx = getContext('2d')
            ctx.setLineDash([2, 2]);
            ctx.lineWidth = 2*mm
            ctx.strokeStyle = "white"
            canvas.ctx.clearRect(0, 0, canvas.width, canvas.height)
            ctx.beginPath()
            ctx.moveTo(parent.width * 0.5, 0)
            ctx.lineTo(parent.width * 0.5, parent.height)
            ctx.stroke()
            //Dashed Line End.
        }
    }//Canvas*/


    //Use a Repeater instead of Canvas because of Qt 5.6 bug ? with Canvas.setLineDash()
    property real wid: 2*mm
    property real hei: 4*mm
    property int num: (parent.height-textScore.height-defaultMargins) / (2*hei)+1
    Column {
        //width: defaultMargins*2
        //height: parent.height
        anchors.top: parent.top
        anchors.bottom: textScore.top
        //anchors.bottomMargin: defaultMargins

        visible: ballTimer.running
        anchors.horizontalCenter: parent.horizontalCenter
        Repeater {
            model: num
            Item {
                width: wid; height: hei*2
                Rectangle {
                    width: wid; height: hei
                    color: "white"
                    anchors.top: parent.top
                }
                Rectangle {
                    width: wid; height: hei
                    color: "black"
                    anchors.bottom: parent.bottom
                }
            }
        }//Repeater
    }//Column

    property real paddleSize: 12*mm.toFixed(2)
    Rectangle {
        id: player1
        width: defaultMargins*1.5
        height: paddleSize
        //radius: defaultMargins
        x: parent.width*0.1
        y: parent.height*0.5-height*0.5

        property real xCenter: x+width*0.5
        property real yCenter: y+height*0.5

        signal move(real moveX, real moveY)
        onMove: {
            y -= moveY
            if (y < 0) y=0
            if (y > parent.height-player1.height) y = parent.height-player1.height
        }
    }

    Rectangle {
        id: playerPC
        width: player1.width
        height: paddleSize
        //radius: defaultMargins
        x: parent.width*0.9-width
        y: parent.height*0.5-height*0.5
        property point topLeft: Qt.point(x,y)
        property point bottomRight: Qt.point(x+width,y+height)

        property real xCenter: x+width*0.5
        property real yCenter: y+height*0.5

        function move(){
            //console.log("yMain: "+yMain)
            //console.log("yCenter: "+yCenter)
            if (ball.vx < 0)
                moveToCenter()
            else
                moveToBall()

        }

        function moveToBall() {

            if ( Math.abs(ball.yCenter - yCenter) > velocityAI ) {

                if (ball.yCenter > yCenter)
                    y += velocityAI
                else
                    y -= velocityAI

            }
        }

        function moveToCenter() {

            if ( Math.abs(playerPC.yCenter - yMain) > velocityAI*2 ){

                if (playerPC.yCenter < yMain)
                    y += velocityAI
                else
                    y -= velocityAI
            }

        }
    }//Rectangle playerPC


    //Ball-----------------------------------------------
    Rectangle {
        id: ball
        width: 4*mm
        height: width
        radius: width*0.5
        color: "white"
        x: parent.width *0.5-width*0.5
        y: parent.height*0.5
        property real xCenter: x+width*0.5
        property real yCenter: y+height*0.5

        property point topLeft: Qt.point(x,y)
        property point bottomRight: Qt.point(x+width,y+height)

        property real vx: 0
        property real vy: 0
        property real a
        property real rand
        property real angle

        //property real velocityInitial: 1*mm.toFixed(2)
        //property real velocity: 1*mm.toFixed(2)


        Timer {
            id: ballTimer
            interval: fps_ms; running: false; repeat: true
            onTriggered: {
                ball.move()
                playerPC.move()
                //ball.velocity += ball.velocityInitial*0.0005
                //console.log("ball.velocity: "+ball.velocity)
            }
        }

        Timer{
            id:timerVelocityIncrease
            interval: 5000; running: ballTimer.running; repeat: true
            onTriggered: {
                //ball.velocity *= 1.05
                velocity *= 1.05
                ball.vx *= 1.05
                ball.vy *= 1.05
            }
        }

        function init(){
            //var angle = (Math.random() * Math.PI*0.5) + Math.PI*0.75  //Math.random() * Math.PI

            a = Math.PI*0.3
            rand = (Math.random() * a) - a/2
            angle = rand //+ Math.PI
            //var velocity = 2*mm
            vx = Math.cos(angle) * velocity
            vy = Math.sin(angle) * velocity
            x = parent.width *0.5-width*0.5
            y = parent.height*0.5
            //ball.velocity = ball.velocityInitial
            velocity = velocityInitial

            ballTimer.start()
        }

        function move() {
           // console.log("move x,y: "+x+","+y)
            x += vx //* velocity
            y += vy //* velocity

            if (y > (parent.height-height)) {
                //if (bSound)
                  //  sound2.play()
                vy = -Math.abs(vy)
            }

            if (y < 0) {
                //if (bSound)
                  //  sound2.play()
                vy = Math.abs(vy)
            }


            /*if (x > (parent.width-width))
                vx = -Math.abs(vx)
            if (x < 0)
                vx = Math.abs(vx)

            return;*/


            if (isPressed && paddle1Animation.running === false) {
                player1.y  = mouseA.mouseY - player1.height*0.5

            }




            //not sure if this will increase performance, return right now if x is fair away from the paddles
            //so it avoids checking thoose Math.abs(.....)
            if (x > performanceX1 && x < performanceX2) {
                return;
            }


            //check right paddle collision
            if ( Math.abs(ball.xCenter - playerPC.xCenter) < (ball.width+playerPC.width)/2
                 && Math.abs(ball.yCenter - playerPC.yCenter) < (ball.height+playerPC.height)/2 )
            {
                vx = -Math.abs(vx)//this makes a angle perfect bounce, delete the folow
                ball.x = playerPC.x - ball.width*1.1 // avoid errors from rounding numbers

                var radAngleBall = getAngle(vx, vy) //ball angle after perfect bouce
                var radAnglePaddle2Ball = getAngle(ball.xCenter-playerPC.xCenter, ball.yCenter-playerPC.yCenter)
                var radAnglePaddle2BallNormalized = radAnglePaddle2Ball-Math.PI;
                //radAnglePaddle2BallNormalized *= 0.7; //reduce angle to 80% effect of the collision
                var newRadAngleBall = radAngleBall + radAnglePaddle2BallNormalized;
                vx = Math.cos(radAnglePaddle2BallNormalized) * velocity
                vy = Math.sin(radAnglePaddle2BallNormalized) * velocity

                if (bSound)
                    sound.play()
            }

            //check left paddle collision
            if ( Math.abs(ball.xCenter - player1.xCenter) < (ball.width+player1.width)/2
                 && Math.abs(ball.yCenter - player1.yCenter) < (ball.height+player1.height)/2 )
            {
                vx = Math.abs(vx)//this makes a angle perfect bounce
                //console.log("bounce rad: "+getAngle(vx, vy))
                //console.log("bounce degree: "+getAngle(vx, vy)/(180*Math.PI))

                ball.x = player1.x + player1.width + ball.width*0.1 // avoid errors from rounding numbers

                var radAngleBall2 = getAngle(vx, vy) //ball angle after perfect bouce
                var radAnglePaddle2Ball2 = getAngle(ball.xCenter-player1.xCenter, ball.yCenter-player1.yCenter)
                //console.log("radAnglePaddle2Ball2: "+radAnglePaddle2Ball2)
                //console.log("degreeAnglePaddle2Ball2: "+radAnglePaddle2Ball2/(180*Math.PI))

                var radAnglePaddle2BallNormalized2 = radAnglePaddle2Ball2;//-Math.PI;
                //radAnglePaddle2BallNormalized2 *= 0.7; //reduce angle to 80% effect of the collision
                var newRadAngleBall2 = radAngleBall2 + radAnglePaddle2BallNormalized2;
                vx = Math.cos(radAnglePaddle2BallNormalized2) * velocity
                vy = Math.sin(radAnglePaddle2BallNormalized2) * velocity

                score++

                if (bSound)
                    sound.play()
            }

            //check left wall collision
            if (ball.x < 0)
                leftWallCollision()

            //check right wall collision
            if (ball.x > parent.width-ball.width)
                rightWallCollision()



        }//function move

    }//Ball-------------------------------------------------------


    property real velocityPowerUPs: 0.2*mm
    property real t: 0
    property real dt: 0.1
    /*Timer {
        id: timerPowerUPsTriger
        interval: 5000; running: ballTimer.running; repeat: true
        onTriggered: {
            powerUP1.x -= velocityPowerUPs
            powerUP1.y = yMain+ parent.height*0.2*Math.cos()
        }
    }*/


    /*Rectangle {
        id: powerUP1

        width: 4*mm
        height: width
        //radius: width*0.5
        color: "lime"
        x: parent.width//parent.width *0.5-width*0.5
        y: parent.height*0.5

        Timer {
            id: timerPowerUPs
            interval: fps_ms; running: ballTimer.running; repeat: true
            onTriggered: {
                powerUP1.x -= velocityPowerUPs
                powerUP1.y = yMain + pong.height*0.2*Math.cos(t*0.5)
                t += dt
            }
            onRunningChanged: {
                powerUP1.x = pong.width//parent.width *0.5-width*0.5
                powerUP1.y = pong.height*0.5
                //t = 0
            }
        }

    }*/

    function getAngle(dx, dy){
        var rad = Math.atan(dy/dx);   //radians
        return rad;
        //var degrees = rad/(180*Math.PI);  //degrees
        //return Math.floor(degrees); //round number, avoid decimal fragments
    }


    Text {
        id: textplayer1Victories
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.3
        text: player1Victories
        font.pixelSize: 8*mm
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: textplayerPCVictories
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.7
        text: playerPCVictories
        font.pixelSize: textplayer1Victories.font.pixelSize
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: textInfo
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: textplayer1Victories.bottom
        anchors.topMargin: defaultMargins
        text: "Double tap\nto start."
        font.pixelSize: textplayer1Victories.font.pixelSize
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        visible: !ballTimer.running
    }

    Text{
        id:textVictory
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: textScore.top
        anchors.bottomMargin: defaultMargins
        text: "You win!!!"
        font.pixelSize: textplayer1Victories.font.pixelSize
        color: "white"
        visible: false
    }



    Text {
        id: textScore
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Score: ") + score + qsTr(" - Record: ")+scoreMax
        font.pixelSize: 5*mm
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }

    PropertyAnimation {
        id: paddle1Animation
        target: player1;property: "y";//to: 30;
        duration: 200
    }

    PropertyAnimation {
        id: paddlePCAnimation
        target: playerPC;property: "y";to: yMain
        duration: 200
    }

    property bool isPressed: false
    MouseArea {
        id: mouseA
        anchors.fill: parent
        /*drag.target: player1
        drag.minimumY: 0//parent.height - sea.height - boat.height*0.75
        drag.maximumY: parent.height - player1.height
        drag.axis: Drag.YAxis
        */

        onDoubleClicked: {
            if (!ballTimer.running) {
                ball.init()
                textVictory.visible = false
                if (player1Victories >= matchPlays || playerPCVictories >= matchPlays )
                    score = player1Victories = playerPCVictories = 0
            }
        }

        onPressed: {
            paddle1Animation.to = mouseA.mouseY - player1.height*0.5
            paddle1Animation.start()
            isPressed = true
        }
        onReleased: isPressed = false
    }

    property bool bSound: true
    Audio {
        id: sound
        source: "qrc:/sounds/clap.ogg"
    }

    Audio {
        id: sound2
        source: "qrc:/sounds/whistle.ogg"
    }

    ButtonEG {
        id: btnSound
        width: 20 * mm
        height: 10 * mm
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: defaultMargins
        text: bSound ? qsTr("Sound Off") : qsTr("Sound On")
        textColor: "white"
        backgroundColor: "black"
        onButtonClick: {
            bSound = !bSound
            //console.log("sound.source: "+sound.source)
            //sound.hasAudio ? sound.source = "" : sound.source = "qrc:/sounds/clap.ogg"
        }
        visible: !ballTimer.running
    }

    ButtonEG {
        id: btnSettings
        width: 20 * mm
        height: 10 * mm
        anchors.top: btnSound.bottom
        anchors.margins: defaultMargins
        anchors.horizontalCenter: btnSound.horizontalCenter
        text: qsTr("Settings")
        textColor: "white"
        backgroundColor: "black"
        onButtonClick: pongSettings.visible = true
        visible: !ballTimer.running
    }

    ButtonEG {
        id: btnQuit
        width: 20 * mm
        height: 10 * mm
        anchors.top: btnSettings.bottom
        anchors.margins: defaultMargins
        anchors.horizontalCenter: btnSound.horizontalCenter
        text: qsTr("Quit")
        textColor: "white"
        backgroundColor: "black"
        onButtonClick: quit()
        visible: !ballTimer.running
    }

    function checkCollison(obj1TopLeft, obj1BottomRight , obj2TopLeft, obj2BottomRight) {
        return rectanglesOverlap(obj1TopLeft, obj1BottomRight , obj2TopLeft, obj2BottomRight) //obj2 bottom right
    }

    function rectanglesOverlap(pL_, pR_, oL, oR) {
        // If one rectangle is on left side of other
        //console.log("rectangles overlap---------------")
        //console.log("obstacle top left xy: "+oL.x+" : "+oL.y)
        //console.log("obstacle bottom right xy: "+oR.x+" : "+oR.y)
        if (pR_.x < oL.x || oR.x < pL_.x) {
            return false;
        }

        // If one rectangle is above other
        if (pR_.y < oL.y || oR.y < pL_.y) {
            return false;
        }
        return true;
    }


    property int matchPlays: 10
    onLeftWallCollision: {
        ballTimer.stop()
        paddlePCAnimation.start()
        playerPCVictories++
        if (playerPCVictories >= matchPlays)
            playerPCWin()
    }

    onRightWallCollision: {
        ballTimer.stop()
        paddlePCAnimation.start()
        player1Victories++
        if (player1Victories >= matchPlays)
            player1Win()
    }

    onPlayer1Win: {
        //ballTimer.stop()
        textVictory.text = "You win!!!"
        textVictory.visible = true
        if (scoreMax < score)
            scoreMax = score
    }

    onPlayerPCWin: {
        //ballTimer.stop()
        textVictory.text = "You lose!!!"
        textVictory.visible = true
    }

    //----------------------------------------------------------------
    PongSettings {
        id: pongSettings
        visible: false
        anchors.fill: parent

        //intputBallVelocity: ball.velocityInitial
        //onIntputBallVelocityChanged: ball.velocity = ball.velocityInitial = intputBallVelocity
        intputBallVelocity: velocityInitial
        onIntputBallVelocityChanged: velocity = velocityInitial = intputBallVelocity

        inputPaddleAIVelocity: velocityAI
        onInputPaddleAIVelocityChanged: velocityAI = inputPaddleAIVelocity

        intputPaddleSize: paddleSize
        onIntputPaddleSizeChanged: paddleSize = pongSettings.intputPaddleSize

        inputMatchPlays: matchPlays
        onInputMatchPlaysChanged: matchPlays = pongSettings.inputMatchPlays

        onOk: visible = false

        onRestoreDefautls: {
            velocity = velocityInitial = intputBallVelocity = 1*mm.toFixed(2)
            velocityAI = inputPaddleAIVelocity = 1.2*mm.toFixed(2)
            paddleSize = pongSettings.intputPaddleSize = 12*mm.toFixed(2)
            matchPlays = pongSettings.inputMatchPlays = 10
        }
    }

}
