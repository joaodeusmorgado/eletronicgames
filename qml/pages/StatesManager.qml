import QtQuick 2.0

import "components"

Item {
    id: rootStatesManager
    width: mainRoot.width
    height: mainRoot.height

    //property alias pong: pong

    property alias velocity: pong.velocity
    property alias velocityInitial: pong.velocityInitial
    property alias velocityAI: pong.velocityAI
    property alias paddleSize: pong.paddleSize
    property alias scoreMax: pong.scoreMax
    property alias matchPlays: pong.matchPlays

    state: "PageInitial"

    states: [
        State {
            name: "PageInitial"
            PropertyChanges {target: pageInitial; visible: true }
        },
        State {
            name: "PageMenu"
            PropertyChanges {target: pageMenu; visible: true }
        },
        State {
            name: "PageAbout"
            PropertyChanges {target: pageAbout; visible: true }
        },
        State {
            name: "Pong"
            PropertyChanges {target: pong; visible: true }
        }
        /*,State {
            name: "PongSettings"
            PropertyChanges {target: pongSettings; visible: true }
        }*/
    ]

    PageInitial{
        id: pageInitial
        visible: false
        onStart: rootStatesManager.state = "PageMenu"
        onSettings: rootStatesManager.state = "PageAbout"
    }

    PageMenu{
        id: pageMenu
        visible: false
        onBack: rootStatesManager.state = "PageInitial"
        onPong: rootStatesManager.state = "Pong"
    }

    PageAbout{
        id: pageAbout
        visible: false
        onBack: rootStatesManager.state = "PageInitial"
    }

    Pong{
        id: pong
        visible: false
        anchors.fill: parent
        onStart: commandKeyb.move.connect(player1.move)
        onEnd: commandKeyb.move.disconnect(player1.move)
        onQuit: rootStatesManager.state = "PageInitial"
        //onPongOptions: rootStatesManager.state = "PongSettings"
    }



    CommandKeyboard {
        id: commandKeyb
    }
}
