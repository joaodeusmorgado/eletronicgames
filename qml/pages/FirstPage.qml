import QtQuick 2.0
import QtQuick.Window 2.2
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0 //for sailfish OS

Page {
    id: mainRoot

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    //Define buttons and text size in milimeters.
    property real mm: Screen.pixelDensity

    //Define button size.
    property real buttonHeight: 10 * mm
    property real buttonWidth:  30 * mm
    property real textInpuHeight: 7 * mm

    //Define margins.
    property real defaultMargins: 2 * mm

    //Define text size.
    property int textSize: 4*mm
    property int textTitleSize: 10*mm
    property int fps: 60
    property int fps_ms: 1000/fps


    ConfigurationGroup {
        id: settings
        path: "/apps/harbour-eletronicgames"
    }

    Component.onCompleted: {
        statesManager.velocity = settings.value("velocity", 1*mm.toFixed(2))
        statesManager.velocityInitial = settings.value("velocityInitial", 1*mm.toFixed(2))
        statesManager.velocityAI = settings.value("velocityAI", 1*mm.toFixed(2))
        statesManager.paddleSize = settings.value("paddleSize", 12*mm.toFixed(2))
        statesManager.scoreMax = settings.value("scoreMax", 0)
        statesManager.matchPlays = settings.value("matchPlays", 10)
    }

    Component.onDestruction: {
        settings.setValue("velocity", statesManager.velocity)
        settings.setValue("velocityInitial", statesManager.velocityInitial)
        settings.setValue("velocityAI", statesManager.velocityAI)
        settings.setValue("paddleSize", statesManager.paddleSize)
        settings.setValue("scoreMax", statesManager.scoreMax)
        settings.setValue("matchPlays", statesManager.matchPlays)
    }

    StatesManager {
        id: statesManager
        anchors.fill: parent
    }

}
