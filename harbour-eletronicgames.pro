# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-eletronicgames

CONFIG += sailfishapp

SOURCES += src/harbour-eletronicgames.cpp

DISTFILES += qml/harbour-eletronicgames.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    #qml/pages/Pong.qml \
    #qml/pages/SecondPage.qml \
    #qml/pages/StatesManager.qml \
    rpm/harbour-eletronicgames.changes.in \
    rpm/harbour-eletronicgames.changes.run.in \
    rpm/harbour-eletronicgames.spec \
    rpm/harbour-eletronicgames.yaml \
    translations/*.ts \
    harbour-eletronicgames.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-eletronicgames-de.ts

RESOURCES += \
    qml.qrc \
    sounds.qrc
